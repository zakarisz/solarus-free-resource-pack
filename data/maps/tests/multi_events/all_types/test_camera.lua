--[[
	This unit test for the multi_events script verifies that the camera:on_state_changed
	event gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local camera = map:get_camera()
camera:register_event("on_state_changed", function(self, new_state_name)
	events_proto:log(sol.main.get_type(self)..","..new_state_name)
end)

function map:on_opening_transition_finished()
	local end_test_cb
	events_proto:set_trigger(function(callback)
		end_test_cb = callback
		camera:start_manual()
		return true
	end)
	events_proto:trigger"camera,manual"
	
	sol.timer.start(self, 10, function()
		end_test_cb()
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
