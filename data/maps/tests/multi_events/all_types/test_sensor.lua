--[[
	This unit test for the multi_events script verifies that the sensor:on_activated()
	event gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local end_test_cb
sensor:register_event("on_activated", function(self)
	events_proto:log(sol.main.get_type(self))
	end_test_cb()
	events_proto:exit() --verifies all tests have finished running before exit
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function(callback)
		end_test_cb=callback
		game:simulate_command_pressed"right" --hero moves over sensor
		return true
	end)
	events_proto:trigger"sensor"
	
	sol.timer.start(map, 2000, function() --if event not triggered after 2 sec then exit (test fails)
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
