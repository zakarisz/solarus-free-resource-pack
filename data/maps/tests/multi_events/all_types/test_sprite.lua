--[[
	This unit test for the multi_events script verifies that the
	sprite:on_direction_changed event gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local sprite = sol.sprite.create"hero/tunic1"
sprite:register_event("on_direction_changed", function(self, animation, direction)
	local data = {sol.main.get_type(self), animation, direction}
	events_proto:log(table.concat(data, ","))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function()
		sprite:set_direction(1 - sprite:get_direction()) --toggle between direction 1 & 0
	end)
	events_proto:trigger"sprite,stopped,1"
	
	events_proto:exit() --verifies all tests have finished running before exit
end
