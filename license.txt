﻿We give in this file the license information of
the Solarus Free Resource Pack.

Some resources come from communities like OpenGameArt, and others are original
creations made by us, the Solarus team.

- Lua scripts are licensed under the terms of the GNU General Public License
in version 3.

- Most data files other than Lua scripts are licensed under
Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0).
http://creativecommons.org/licenses/by-sa/4.0/

- A few data files are in public domain.

Main contributors:
- Christopho
- Diarandor <diarandor at gmail dot com>:
  - Source URL for images: http://diarandor.deviantart.com/
  - Source URL for sounds and music: https://soundcloud.com/diarandor/
- Eduardo Dueñas <duenase@hotmail.com>
- Olivier Cléro
- Bertram
- Max Mraz
- Alex Gleason
- Zane Kukta

More detailed information about the authors and license of every file
is displayed when you open this project with Solarus Quest Editor.

